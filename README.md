# Java Compare Microservice and Vendor
The package preconfigures the boilerplate code with the feature to check transaction status between vendor through api and microservice through database. Each compared records are saved in recheck_transactions table.

## Installation
You can install this package by using the following dependency in your pom.xml file:
```
<dependency>
  <groupId>com.gitlab.baniyaavaya</groupId>
  <artifactId>compare-microservice-and-vendor-java</artifactId>
  <version>1.5.0</version>
</dependency>
```

Add the following annotations in your application class (class using the ``@SpringBootApplication`` annotation), to load the package's configuration and enable JPA repositories and entities that lie outside the base package
```
@Import(value = {CompareConfiguration.class})
@EnableJpaRepositories("com.*")
@EntityScan("com.*")
```


## How to use
### Implementing Microservice Database checks

An abstract implementation, ``DbStatusCompareAbsttract`` abstract class, implementing ``MicroserviceDbStatusContract`` interface can be used to implement database checks

### Implementing Vendor API checks

An abstract implementation, ``ApiStatusCompareAbstract`` abstract class, implementing ``VendorApiStatusContract`` interface can be used to implement api checks

### Comparing Db and Api checks
Api and Db checks can be compared by calling the ``compareApiAndDb`` method in ``RecheckTransactionService`` class which returns an instance of ``CompareApiAndDb`` object; values regarding the transaction can be set using the setter methods of this class.






