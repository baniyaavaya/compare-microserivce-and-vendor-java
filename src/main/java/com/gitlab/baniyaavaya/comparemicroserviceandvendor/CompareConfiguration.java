package com.gitlab.baniyaavaya.comparemicroserviceandvendor;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class CompareConfiguration {

}
