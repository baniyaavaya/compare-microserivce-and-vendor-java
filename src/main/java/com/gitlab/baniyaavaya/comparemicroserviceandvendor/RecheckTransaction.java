package com.gitlab.baniyaavaya.comparemicroserviceandvendor;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "recheck_transactions")
@Entity
public class RecheckTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "pre_transaction_id", nullable = false)
    private String preTransactionId;

    @Column(name = "vendor_id", nullable = false)
    private String vendorId;

    @Column(name = "user_id")
    private int userId;

    @Column(name = "recheck_transaction_type")
    private String recheckTransactionType;

    @Column(name = "previous_microservice_status")
    private String previousMicroserviceStatus;

    @Column(name = "previous_vendor_status")
    private String previousVendorStatus;

    @Column(name = "microservice_status", nullable = false)
    private String microserviceStatus;

    @Column(name = "vendor_status", nullable = false)
    private String vendorStatus;

    @Column(name = "json_request", columnDefinition = "TEXT")
    private String jsonRequest;

    @Column(name = "json_response", columnDefinition = "TEXT")
    private String jsonResponse;

    private String special1;
    private String special2;
    private String special3;
    private String special4;

    @Column(columnDefinition="TEXT")
    private String text1;
    @Column(columnDefinition="TEXT")
    private String text2;

    @CreatedDate
    @DateTimeFormat(pattern = "yyyy-mm-dd HH:mm:ss")
    @Column(name = "created_at")
    private Date createdAt;

    @LastModifiedDate
    @DateTimeFormat(pattern = "yyyy-mm-dd HH:mm:ss")
    @Column(name = "updated_at")
    private Date updatedAt;

    @PrePersist
    protected void prePersist(){
        if (this.createdAt == null) this.createdAt = new Date();
        if (this.updatedAt ==null) this.updatedAt = new Date();
    }

    @PreUpdate
    protected void preUpdate(){
        this.updatedAt = new Date();
    }
}
