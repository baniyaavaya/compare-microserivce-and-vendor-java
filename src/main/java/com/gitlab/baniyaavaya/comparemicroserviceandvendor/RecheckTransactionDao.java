package com.gitlab.baniyaavaya.comparemicroserviceandvendor;

import com.gitlab.baniyaavaya.comparemicroserviceandvendor.compare.CompareApiAndDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RecheckTransactionDao {

    private final RecheckTransactionRepository recheckTransactionRepository;

    @Autowired
    public RecheckTransactionDao(RecheckTransactionRepository recheckTransactionRepository) {
        this.recheckTransactionRepository = recheckTransactionRepository;
    }


    public RecheckTransaction createRecheckTransaction(CompareApiAndDb compare) {

        RecheckTransaction recheckTransaction = new RecheckTransaction();
        recheckTransaction.setPreTransactionId(compare.getPreTransactionId());
        recheckTransaction.setVendorId(compare.getVendorId());
        recheckTransaction.setUserId(compare.getUserId());
        recheckTransaction.setRecheckTransactionType(compare.getRecheckTransactionType());
        recheckTransaction.setMicroserviceStatus(compare.getDbStatus().getCurrentStatus());
        recheckTransaction.setVendorStatus(compare.getApiStatus().getCurrentStatus());
        recheckTransaction.setSpecial1(compare.getSpecial1());
        recheckTransaction.setSpecial2(compare.getSpecial2());
        recheckTransaction.setSpecial3(compare.getSpecial3());
        recheckTransaction.setSpecial4(compare.getSpecial4());
        recheckTransaction.setText1(compare.getText1());
        recheckTransaction.setText2(compare.getText2());
        recheckTransaction.setJsonRequest(compare.getApiStatus().getJsonRequest());
        recheckTransaction.setJsonResponse(compare.getApiStatus().getJsonResponse());

        return this.recheckTransactionRepository.save(recheckTransaction);
    }

}
