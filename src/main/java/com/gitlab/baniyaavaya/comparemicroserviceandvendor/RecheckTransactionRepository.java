package com.gitlab.baniyaavaya.comparemicroserviceandvendor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecheckTransactionRepository extends JpaRepository<RecheckTransaction, Long> {
}
