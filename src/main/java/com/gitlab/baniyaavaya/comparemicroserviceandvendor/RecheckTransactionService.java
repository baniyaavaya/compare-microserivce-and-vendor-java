package com.gitlab.baniyaavaya.comparemicroserviceandvendor;

import com.gitlab.baniyaavaya.comparemicroserviceandvendor.compare.CompareApiAndDb;
import com.gitlab.baniyaavaya.comparemicroserviceandvendor.compare.contracts.MicroserviceDbStatusContract;
import com.gitlab.baniyaavaya.comparemicroserviceandvendor.compare.contracts.VendorApiStatusContract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecheckTransactionService {

    public RecheckTransactionDao recheckTransactionDao;

    @Autowired
    public RecheckTransactionService(RecheckTransactionDao recheckTransactionDao) {
        this.recheckTransactionDao = recheckTransactionDao;
    }


    public CompareApiAndDb compareApiAndDb(String preTransactionId, VendorApiStatusContract apiStatus, MicroserviceDbStatusContract dbStatus) {

        CompareApiAndDb compareApiAndDb = new CompareApiAndDb(preTransactionId, apiStatus, dbStatus);
        compareApiAndDb.setRecheckTransactionDao(this.recheckTransactionDao);
        return compareApiAndDb;
    }
}
