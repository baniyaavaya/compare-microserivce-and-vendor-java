package com.gitlab.baniyaavaya.comparemicroserviceandvendor.compare;

import com.gitlab.baniyaavaya.comparemicroserviceandvendor.compare.contracts.VendorApiStatusContract;

abstract public class ApiStatusCompareAbstract implements VendorApiStatusContract {

    protected String currentStatus;

    protected String jsonRequest;

    protected String jsonResponse;

    //current status
    public String getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    //jsonRequest
    public String getJsonRequest() {
        return this.jsonRequest;
    }

    public void setJsonRequest(String jsonRequest) {
        this.jsonRequest = jsonRequest;
    }

    //jsonResponse
    public String getJsonResponse() {
        return this.jsonResponse;
    }

    public void setJsonResponse(String jsonResponse) {
        this.jsonResponse = jsonResponse;
    }


    //check status
     abstract public void checkStatus(String $preTransactionId);
}
