package com.gitlab.baniyaavaya.comparemicroserviceandvendor.compare;

import com.gitlab.baniyaavaya.comparemicroserviceandvendor.RecheckTransaction;
import com.gitlab.baniyaavaya.comparemicroserviceandvendor.RecheckTransactionDao;
import com.gitlab.baniyaavaya.comparemicroserviceandvendor.compare.contracts.MicroserviceDbStatusContract;
import com.gitlab.baniyaavaya.comparemicroserviceandvendor.compare.contracts.VendorApiStatusContract;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompareApiAndDb {

    private String vendorId;
    private int userId;

    private String recheckTransactionType;

    private String special1;
    private String special2;
    private String special3;
    private String special4;

    private String text1;
    private String text2;

    private final String preTransactionId;

    private VendorApiStatusContract apiStatus;

    private MicroserviceDbStatusContract dbStatus;

    private RecheckTransactionDao recheckTransactionDao;

    public CompareApiAndDb(String preTransactionId, VendorApiStatusContract apiStatus, MicroserviceDbStatusContract dbStatus) {
        this.preTransactionId = preTransactionId;
        this.apiStatus = apiStatus;
        this.dbStatus = dbStatus;
    }

    public RecheckTransaction compare() {

        this.dbStatus.checkStatus(this.preTransactionId);
        this.apiStatus.checkStatus(this.preTransactionId);

        return this.recheckTransactionDao.createRecheckTransaction(this);
    }
}
