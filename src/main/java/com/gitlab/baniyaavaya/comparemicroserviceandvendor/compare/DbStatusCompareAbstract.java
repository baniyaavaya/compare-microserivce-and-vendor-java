package com.gitlab.baniyaavaya.comparemicroserviceandvendor.compare;

import com.gitlab.baniyaavaya.comparemicroserviceandvendor.compare.contracts.MicroserviceDbStatusContract;

abstract public class DbStatusCompareAbstract implements MicroserviceDbStatusContract {

    protected String currentStatus;

    public String getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    abstract public void checkStatus(String preTransactionId);
}
