package com.gitlab.baniyaavaya.comparemicroserviceandvendor.compare.contracts;

public interface MicroserviceDbStatusContract {

    public String getCurrentStatus();

    public void setCurrentStatus(String status);

    public void checkStatus(String preTransactionId);
}
