package com.gitlab.baniyaavaya.comparemicroserviceandvendor.compare.contracts;

public interface VendorApiStatusContract {

    //current status
    public String getCurrentStatus();

    public void setCurrentStatus(String currentStatus);

    //jsonRequest
    public String getJsonRequest();

    public void setJsonRequest(String jsonRequest);

    //jsonResponse
    public String getJsonResponse();

    public void setJsonResponse(String jsonResponse);


    //check status
    public void checkStatus(String $preTransactionId);


}
